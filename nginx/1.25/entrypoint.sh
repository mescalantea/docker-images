#!/bin/bash

export DOLLAR='$'

if [ "$NGINX_CONF" = "wordpress" ]; then
  template="wordpress.conf.template"
else
  template="default.conf.template"
fi

# Replace the environment variables in the template
envsubst < /etc/nginx/conf.d/"$template" > /etc/nginx/conf.d/default.conf

# Remove unnecessary configuration template files
rm -rf /etc/nginx/conf.d/*.conf.template

# Set the right permissions & ownership
chmod 0644 /etc/nginx/conf.d/default.conf
chown nginx:nginx /etc/nginx/conf.d/default.conf

# Start the nginx server
nginx -g 'daemon off;'
