# Docker images

This repository contains custom Docker images by michelescalantea.

## PHP 8.2 Docker Image

This is a Docker image based on the official PHP 8.2 FPM image with some custom configurations.

### Features

* Customized PHP configuration settings for various parameters such as maximum execution time, memory limit, and more.
* Configuration options for SMTP server settings.
* Integration with Redis for caching and session management.
* Configuration options for connecting to a MySQL database.
* Installed and enabled additional PHP extensions such as intl, bcmath, exif, redis, and imagick.

### Tags

- ```8.2```: PHP-FPM 8.2
- ```8.2-wpcli```: PHP-FPM 8.2 with WP-CLI installed.
- ```8.2-wpcli-dev```: PHP-FPM 8.2 with WP-CLI + XDebug.
- ```8.2-wp-dev```: PHP-FPM 8.2 with WordPress + WP-CLI + XDebug.

### Usage

To use this Docker image, you can build it locally or pull it from Docker Hub:

```bash
docker pull michelescalantea/php:<tag>
```
Replace with the appropriate image tag.

### Configuration

You can customize the configuration of this Docker image using environment variables. The values shown are the default ones:

```bash
# PHP
MAX_EXECUTION_TIME=30
MAX_INPUT_TIME=60
MAX_INPUT_VARS=1000
MEMORY_LIMIT=128M
POST_MAX_SIZE=64M
UPLOAD_MAX_FILESIZE=64M
MAX_FILE_UPLOADS=20
ALLOW_URL_FOPEN=On
ALLOW_URL_INCLUDE=Off

# SMTP
SMTP_HOST=localhost
SMTP_PORT=25
SENDMAIL_FROM=
SENDMAIL_PATH="/usr/sbin/sendmail -t -i"
SMTP_SERVER=
SMTP_USERNAME=
SMTP_USERPASS=
SMTP_SSL=

# Redis
REDIS_HOST=redis
REDIS_PORT=6379
REDIS_PASSWORD=

# MySQL
MYSQL_HOST=
MYSQL_DATABASE=
MYSQL_USER=
MYSQL_PASSWORD=

# Exclusive for WordPress tag
WP_VERSION=latest
WP_LOCALE=en_US
WP_DB_PREFIX=wp_
WP_DBCHARSET="utf8"
WP_DBCOLLATE=
WP_URL=http://localhost
WP_TITLE=WordPress
WP_ADMIN_USER=admin
WP_ADMIN_PASS=admin
WP_EXTRA_PHP=
WP_ADMIN_EMAIL= # Required
WP_THEME="twentytwentyfour"
WP_PLUGINS=
```

Also, for the WordPress tag you could create an script that will run after WordPress was installed and setup to perform additional configuration actions. If you need this feature, you should mount the script as a volume mapped to ```/usr/local/bin/post-install.sh``` path inside the container. Example:

```yml
version: '3.8'

services:
  wp:
    container_name: wp
    image: "michelescalantea/php:8.2-wp-dev"
    env_file:
      - .env
    volumes:
      - files:/var/www/html
      - ./.devcontainer/post-install.sh:/usr/local/bin/post-install.sh

volumes:
  files:
```

## Apache

Custom Apache image to work together the PHP-FPM image.

### Tags

- ```2.4.58```

### Usage

To use this Docker image, you can build it locally or pull it from Docker Hub:

```bash
docker pull michelescalantea/apache:<tag>
```
Replace with the appropriate image tag.

### Configuration

```bash
APACHE_RUN_USER=www-data
APACHE_RUN_GROUP=www-data
FCGI_HANDLER=php:9000 # Set here the php-fpm container name and port
PROXY_PATH=/var/www/html # The path to your PHP application
```

## NGINX

Custom NGINX image to work together the PHP-FPM image.

### Tags

- ```1.25```

### Usage

To use this Docker image, you can build it locally or pull it from Docker Hub:

```bash
docker pull michelescalantea/nginx:<tag>
```
Replace with the appropriate image tag.

### Configuration

```bash
FCGI_HANDLER=php:9000 # Set here the php-fpm container name and port
```

Feel free to contribute, report issues, or submit pull requests.