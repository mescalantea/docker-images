#!/bin/bash
# This script deletes all images with the given repository name. Use with caution.
# Usage: ./rmi-repo.sh <repository>
# Example: ./rmi-repo.sh michelescalantea/nginx

REPOSITORY="$1"

if [ -z "$REPOSITORY" ]; then
    echo "Usage: ./rmi-repo.sh <repository>"
    echo "Example: ./rmi-repo.sh michelescalantea/nginx"
    exit 1
fi

TAGS=$(docker images --format "{{.Repository}}:{{.Tag}}" | grep "$REPOSITORY")

for TAG in $TAGS; do
    docker rmi $TAG
done