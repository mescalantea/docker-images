#!/bin/sh

# Build and push all the images
# ./php/7.2/build.sh "$1"

# ./php/7.4/build.sh "$1"
# ./php/7.4-wpcli/build.sh "$1"
# ./php/7.4-wpcli-dev/build.sh "$1"

./php/8.2/build.sh "$1"
./php/8.2-wpcli/build.sh "$1"
./php/8.2-wpcli-dev/build.sh "$1"
./php/8.2-wp-dev/build.sh "$1"

# ./php-cron/8.2/build.sh "$1"
# ./apache/2.4.58/build.sh "$1"
./apache/2.4.62/build.sh "$1"
# ./nginx/1.25/build.sh "$1"
./nginx/1.27/build.sh "$1"