FROM php:8.2-fpm

LABEL maintainer="Michel Escalante <hi@michelescalante.com>"
LABEL description="PHP 8.2 FPM image based on official PHP image with some custom configurations."
LABEL repository="https://gitlab.com/mescalantea/docker-images"

# PHP
ENV MAX_EXECUTION_TIME=30
ENV MAX_INPUT_TIME=60
ENV MAX_INPUT_VARS=1000
ENV MEMORY_LIMIT=128M
ENV POST_MAX_SIZE=64M
ENV UPLOAD_MAX_FILESIZE=64M
ENV MAX_FILE_UPLOADS=20
ENV ALLOW_URL_FOPEN=On
ENV ALLOW_URL_INCLUDE=Off

# SMTP
ENV SMTP_HOST=localhost
ENV SMTP_PORT=25
ENV SENDMAIL_FROM=
ENV SENDMAIL_PATH="/usr/sbin/sendmail -t -i"
ENV SMTP_SERVER=
ENV SMTP_USERNAME=
ENV SMTP_USERPASS=
ENV SMTP_SSL=

# Redis
ENV REDIS_HOST=redis
ENV REDIS_PORT=6379
ENV REDIS_PASSWORD=

# Database
ENV MYSQL_HOST=
ENV MYSQL_DATABASE=
ENV MYSQL_USER=
ENV MYSQL_PASSWORD=

ADD php.ini /usr/local/etc/php/php.ini
# Set owner and permissions to user 0 and group 0
RUN chown -R 0:0 /usr/local/etc/php/php.ini && \
    chmod -R 644 /usr/local/etc/php/php.ini

# Install dependencies
RUN apt-get update && apt-get install -y \
    sendmail \
    libzip-dev \
    libonig-dev \
    libmagickwand-dev \
    libhiredis-dev && \
    rm -rf /var/lib/apt/lists/* 

# Install and enable PHP extensions
RUN docker-php-ext-install \
    pdo_mysql \
    mbstring \
    zip \
    mysqli \
    intl \
    bcmath \
    exif

# Install PECL: Redis and Imagick
RUN pecl install redis imagick && \
    docker-php-ext-enable redis imagick

ENTRYPOINT ["php-fpm"]