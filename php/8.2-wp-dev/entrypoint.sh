#!/bin/bash
# Check if need to install WordPress
if [ ! -f /var/www/html/wp-config.php ]; then
    # Wait for database to be ready
    db_port=3306
    until nc -z "$MYSQL_HOST" "$db_port"; do
        >&2 echo "Waiting for database at $MYSQL_HOST:$db_port..."
        sleep 1
    done

    # Generate wp-config.php
    wp config create --allow-root  --force \
    --dbhost="${MYSQL_HOST}" \
    --dbname="${MYSQL_DATABASE}" \
    --dbprefix="${WP_DB_PREFIX}" \
    --dbuser="${MYSQL_USER}" \
    --dbpass="${MYSQL_PASSWORD}" \
    --dbcharset="${WP_DBCHARSET}" \
    --dbcollate="${WP_DBCOLLATE}" \
    --locale="${WP_LOCALE}" \
    --extra-php="${WP_EXTRA_PHP}"

    # Install
    wp core install --allow-root \
    --url="${WP_URL}" \
    --title="${WP_TITLE}" \
    --admin_user="${WP_ADMIN_USER}" \
    --admin_password="${WP_ADMIN_PASS}" \
    --admin_email="${WP_ADMIN_EMAIL}" \
    --locale="${WP_LOCALE}" \
    --skip-email

    # Install themes
    if [ -n "${WP_THEME}" ]; then
        wp theme install --allow-root "${WP_THEME}" --activate
    fi
    # Install plugins
    if [ -n "${WP_PLUGINS}" ]; then
        IFS=',' read -ra plugins <<< "$WP_PLUGINS"
        for plugin in "${plugins[@]}"; do
            wp plugin install --allow-root "$plugin" --activate
        done
    fi

    # Run post-installation scripts
    # Check if /usr/local/bin/post-install.sh exists
    if [ -f /usr/local/bin/post-install.sh ]; then
        chmod +x /usr/local/bin/post-install.sh
        /usr/local/bin/post-install.sh
    fi
fi

# Run php-fpm
php-fpm
