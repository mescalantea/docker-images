#!/bin/bash
# This script deletes all dangling images (with no repository name).
# Usefull to clean up disk space after building images.
# Usage: ./rmi-danging.sh
docker rmi $(docker images -f "dangling=true" -q)