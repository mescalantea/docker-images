#!/bin/sh
# $1 is the docker tag
# $2 is the Dockerfile directory
COMMIT_HASH=$(git rev-parse --short HEAD)

--platform linux/arm64/v8
# PLATFORM="--platform linux/amd64,linux/arm64/v8"
# PLATFORM="--progress=plain --platform linux/amd64,linux/arm/v8,linux/arm64"

# Check if the builder "multiarch-builder" already exists and is inactive
if docker buildx inspect multiarch-builder | grep -q "Status: *inactive"; then
    echo "Activating existing builder 'multiarch-builder'"
    docker buildx inspect multiarch-builder --bootstrap
else
    echo "Creating and using builder 'multiarch-builder'"

    docker buildx stop multiarch-builder
    docker buildx rm multiarch-builder

    docker buildx create --name multiarch-builder --use --driver docker-container --driver-opt image=moby/buildkit:latest,container=multiarch-builder0 "$PLATFORM"
fi

# build images appending the commit hash to the tag
# check if $3 is empty
if [ -z "$3" ]
then
    docker buildx build --load --platform linux/amd64 -t "$1-$COMMIT_HASH" "$2"
    # docker buildx build --load --platform linux/arm64/v8 -t "$1-$COMMIT_HASH" "$2"
    # docker buildx build --push --platform linux/amd64,linux/arm64/v8 -t "$1-$COMMIT_HASH" "$2"
else
    docker buildx build --load --platform linux/amd64 $3 -t "$1-$COMMIT_HASH" "$2"
    # docker buildx build --load --platform linux/arm64/v8 $3 -t "$1-$COMMIT_HASH" "$2"
    # docker buildx build --push --platform linux/amd64,linux/arm64/v8 $3 -t "$1-$COMMIT_HASH" "$2"
fi
docker buildx build --load --platform linux/amd64 -t "$1" "$2"
# docker buildx build --load --platform linux/arm64/v8 -t "$1" "$2"
# docker buildx build --push --platform linux/amd64,linux/arm64/v8 -t "$1" "$2"

# push images to the registry
docker push $1-$COMMIT_HASH
docker push $1